<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlutoAlien
 *
 * @author pabhoz
 */
class PlutoAlien extends BadAlien{
    private $nombre, $edad, $especie;
    private $planeta = "Pluto";
    
    function __construct($nombre, $edad, $especie) {
        parent::__construct($nombre, $edad, $especie, $this->planeta);
    }
}

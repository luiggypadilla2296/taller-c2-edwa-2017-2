<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alien
 *
 * @author pabhoz
 */
class Alien{
    private $nombre, $edad, $especie, $planeta;
    private $moral = "neutral";
    const COMUNICACION = "telepaticamente";
    
    function __construct($nombre, $edad, $especie, $planeta) {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->especie = $especie;
        $this->planeta = $planeta;
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function getEspecie() {
        return $this->especie;
    }

    function getPlaneta() {
        return $this->planeta;
    }

    function getMoral() {
        return $this->moral;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setEspecie($especie) {
        $this->especie = $especie;
    }

    function setPlaneta($planeta) {
        $this->planeta = $planeta;
    }

    function setMoral($moral) {
        $this->moral = $moral;
    }

    public function interact(){
        return self::COMUNICACION." dice: Hola terricola mi nombre es ".$this->getNombre().
                ", vinimos en son de paz";
    }
    
    public function whoIAm(){
        return self::COMUNICACION." dice: 
    Mi nombre es ".$this->getNombre().", vengo del plantea ".$this->getPlaneta().
                ", soy un ".$this->getEspecie()." y soy ".$this->getMoral();
    }
    
}
